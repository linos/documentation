# Gestion des abonnements à travers la fédération

## Page de gestion

En tant qu'administratrice d'un groupe, vous avez la possibilité de gérer les utilisateurs qui [s'abonnent au flux de votre groupe à travers la fédération](../utilisateur et utilisatrice/suivre-federation.md) en cliquant sur&nbsp;:

  1. l'onglet **Mes groupes** sur la barre de navigation supérieure
  1. le nom de votre groupe
  1. **Ajouter / Supprimer…** (dans le bandeau de votre groupe)
  1. l'onglet **Abonné⋅es**

## Rejeter un abonnement

Pour rejeter un abonnement, vous devez, depuis la page de gestion (voir ci-dessus) cliquer sur le bouton **Rejeter** devant l'abonnement que vous souhaitez.
