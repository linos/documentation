# Analytics

Mobilizon can load scripts to provide analytics data. Matomo (previously known as Piwik) and Plausible are supported.

## Matomo

```elixir
config :mobilizon, Mobilizon.Service.FrontEndAnalytics.Matomo,
  enabled: true,
  host: "https://your-matomo-endpoint.org",
  siteId: 1, # The siteId on matomo's side
  trackerFileName: "matomo", # If your endpoint file name is different
  csp: [
    connect_src: ["your-matomo-endpoint.org"],
    script_src: ["your-matomo-endpoint.org"],
    img_src: ["your-matomo-endpoint.org"]
  ]

config :mobilizon, :analytics,
  providers: [
    Mobilizon.Service.FrontEndAnalytics.Matomo
  ]
```

## Plausible

```elixir
config :mobilizon, Mobilizon.Service.FrontEndAnalytics.Plausible,
  enabled: true,
  domain: "my-mobilizon-server.org",
  apiHost: "https://your-plausible-endpoint.org", # Defaults to https://plausible.io
  csp: [
    connect_src: ["your-plausible-endpoint.org"],
    script_src: ["your-plausible-endpoint.org"]
  ]

config :mobilizon, :analytics,
  providers: [
    Mobilizon.Service.FrontEndAnalytics.Plausible
  ]
```


